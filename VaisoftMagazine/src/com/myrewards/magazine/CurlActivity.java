/*
   Copyright 2012 Harri Smatt

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package com.myrewards.magazine;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.myrewards.magazine.curlview.CurlPage;
import com.myrewards.magazine.curlview.CurlView;
import com.myrewards.magazine.curlview.CustomWebView;
import com.myrewards.magazine.model.BookJson;
import com.myrewards.magazine.utils.Utility;

/**
 * Simple Activity for curl testing.
 * 
 * @author harism
 */
public class CurlActivity extends Activity implements OnClickListener {

	private CurlView mCurlView;

	/*
	 * String urls[] = { "pagethumb_0001.jpg", "pagethumb_0002.jpg",
	 * "pagethumb_0003.jpg", "pagethumb_0004.jpg", "pagethumb_0005.jpg",
	 * "pagethumb_0006.jpg", "pagethumb_0007.png", "pagethumb_0008.jpg",
	 * "pagethumb_0009.jpg", "pagethumb_0010.jpg", "pagethumb_0011.jpg",
	 * "pagethumb_0012.jpg", "pagethumb_0013.jpg", "pagethumb_0014.jpg",
	 * "pagethumb_0015.jpg", "pagethumb_0016.jpg", "pagethumb_0017.jpg",
	 * "pagethumb_0018.jpg", "pagethumb_0019.jpg" };
	 * 
	 * String webUrls[] = { "0001.html", "0002.html", "0003.html", "0004.html",
	 * "0005.html", "0006.html", "0007.html", "0008.html", "0009.html",
	 * "0010.html", "0011.html", "0012.html", "0013.html", "0014.html",
	 * "0015.html", "0016.html", "0017.html", "0018.html", "0019.html" };
	 * 
	 * String fiveURLS[] = {
	 * "https://www.google.co.in/?gfe_rd=cr&ei=VGLjU7qeIJDV8geSuIDIBA&gws_rd=ssl"
	 * , "https://in.yahoo.com/?p=us", "https://www.linkedin.com/",
	 * "https://twitter.com/", "http://www.infosys.com/" };
	 * 
	 * String moviePaths[] = {
	 * "file:///storage/emulated/0/MAGAZINE/MOVIE/pagethumb_0001.jpg",
	 * "file:///storage/emulated/0/MAGAZINE/MOVIE/pagethumb_0002.jpg",
	 * "file:///storage/emulated/0/MAGAZINE/MOVIE/pagethumb_0003.jpg",
	 * "file:///storage/emulated/0/MAGAZINE/MOVIE/pagethumb_0004.jpg",
	 * "file:///storage/emulated/0/MAGAZINE/MOVIE/pagethumb_0005.jpg" };
	 */

	Bitmap front, back;
	CurlPage shambhipage;

	private Handler handler;

	LinearLayout favLinear;

	public static HorizontalScrollView hscroll;

	public static int pageIndex;
	public static int pageActualIndex;

	GestureDetector gestureDetector;

	Dialog messageDialog;

	CustomWebView web3;

	private GestureDetector detector;

	private static final int SWIPE_MIN_DISTANCE = 120;
	private static final int SWIPE_THRESHOLD_VELOCITY =200;

	public static int screenHeight;

	public static int screenWidth;

	GestureDetector gd;

	LinearLayout.LayoutParams curlparams;

	Button closeBtn;

	File[] pageFiles;
	List<File> fileList;

	private BookJson jsonBook;

	List<String> magazinesList;

	ScaleGestureDetector scaleGestureDetector;

	int simpleCount = 0;

	public BookJson getJsonBook() {
		return this.jsonBook;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		handler = new Handler();

		int index = 0;
		if (getLastNonConfigurationInstance() != null) {
			index = (Integer) getLastNonConfigurationInstance();
		}

		favLinear = (LinearLayout) findViewById(R.id.favLinearLayoutID);

		hscroll = (HorizontalScrollView) findViewById(R.id.scrollViewID);
		closeBtn = (Button) findViewById(R.id.indexBtnID);

		/*
		 * for (int i = 0; i < 5; i++) loadWebViews(i);
		 */

		setDimensions();

		// gind pubs logic here to get the magazine pges

		Intent intent = getIntent();

		try {
			jsonBook = new BookJson();
			jsonBook.setMagazineName(intent
					.getStringExtra(GindActivity.MAGAZINE_NAME));
			jsonBook.fromJson(intent.getStringExtra(GindActivity.BOOK_JSON_KEY));

			// this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

			// this.setOrientation(jsonBook.getOrientation());
			// this.setPagerView(jsonBook);

			magazinesList = jsonBook.getContents();
			Utility.currentBookList = magazinesList;

			Utility.magazineName = jsonBook.getMagazineName();
			/*
			 * String path = "file://" +
			 * Configuration.getMagazinesDirectory(this
			 * )+"/"+jsonBook.getMagazineName() +
			 * File.separator+"assets/images/pagethumb_0001.jpg";
			 */
			String path = Environment.getExternalStorageDirectory()
					+ "/Android/data/com.myrewards.magazine/files/magazines/"
					+ jsonBook.getMagazineName() + "/assets/images/";

			fileList = new ArrayList<File>();

			File f2 = new File(path);
			pageFiles = f2.listFiles();
			if (pageFiles != null) {
				for (int i = 0; i < pageFiles.length; i++) {
					if (pageFiles[i].getName().contains("pagethumb_")) {
						if (!(fileList.contains(pageFiles[i])))
							fileList.add(pageFiles[i]);
					}
				}
				Collections.sort(fileList);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			Toast.makeText(this, "Not valid book.json found!",
					Toast.LENGTH_LONG).show();
		}

		gestureDetector = new GestureDetector(this, new GestureListener());

		scaleGestureDetector = new ScaleGestureDetector(this,
				new simpleOnScaleGestureListener());

		mCurlView = (CurlView) findViewById(R.id.curl);
		mCurlView.setPageProvider(new PageProvider());
		mCurlView.setSizeChangedObserver(new SizeChangedObserver());
		mCurlView.setCurrentIndex(index);
		mCurlView.setBackgroundColor(0xFF202830);

		curlparams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
				0, 6);
		mCurlView.setLayoutParams(curlparams);
		// This is something somewhat experimental. Before uncommenting next
		// line, please see method comments in CurlView.
		// mCurlView.setEnableTouchPressure(true);

		/*
		 * mCurlView.setOnTouchListener(new OnTouchListener() {
		 * 
		 * @Override public boolean onTouch(View v, MotionEvent ev) {
		 * gestureDetector.onTouchEvent(ev); return false; } });
		 */

		/*
		 * fileList=new ArrayList<File>();
		 * 
		 * String fPath= Environment.getExternalStorageDirectory()
		 * .getAbsolutePath() + "/MAGAZINE/MOVIE/"; File f2=new File(fPath);
		 * pageFiles=f2.listFiles(); for(int i=0;i<pageFiles.length;i++) {
		 * if(pageFiles[i].getName().contains("pagethumb_")) {
		 * if(!(fileList.contains(pageFiles[i]))) fileList.add(pageFiles[i]); }
		 * } System.out.print(fileList.size());
		 */

		for (int i = 0; i < fileList.size(); i++) {

			ImageView img = new ImageView(this);

			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
					screenWidth / 5, screenHeight / 8);

			img.setId(i);
			params.setMargins(5, 5, 5, 5);

			img.setLayoutParams(params);

			/*
			 * String filepath = Environment.getExternalStorageDirectory()
			 * .getAbsolutePath() + "/MAGAZINE/MOVIE/" + urls[i]; File f = new
			 * File(filepath);
			 */
			try {
				Bitmap bitmap = decodeScaledBitmapFromSdCard(fileList.get(i)
						.getAbsolutePath(), 100, 100);

				/*
				 * Bitmap bitmap = BitmapFactory.decodeFile(fileList.get(i)
				 * .getAbsolutePath());
				 */

				img.setImageBitmap(bitmap);
			} catch (OutOfMemoryError e) {
				if (e != null)
					e.printStackTrace();
			}
			img.setOnClickListener(this);

			/*
			 * img.setOnClickListener(new OnClickListener() {
			 * 
			 * @Override public void onClick(View v) {
			 * 
			 * mCurlView.setCurrentIndex(v.getId());
			 * 
			 * } });
			 */

			favLinear.addView(img);

		}
		/*
		 * mCurlView.setOnLongClickListener(new View.OnLongClickListener() {
		 * 
		 * @Override public boolean onLongClick(View v) { //
		 * Toast.makeText(CurlActivity.this, "yes", 2000).show();// TODO
		 * Auto-generated method stub
		 * 
		 * if (hscroll.getVisibility() == View.VISIBLE) {
		 * hscroll.setVisibility(View.GONE); curlparams.weight=6;
		 * mCurlView.setLayoutParams(curlparams); } else { curlparams.weight=5;
		 * mCurlView.setLayoutParams(curlparams);
		 * hscroll.setVisibility(View.VISIBLE); }
		 * 
		 * return false; } });
		 */

		/*
		 * hscroll.setOnTouchListener(new OnTouchListener() {
		 * 
		 * @Override public boolean onTouch(View v, MotionEvent event) {
		 * 
		 * return new GestureDetector( new
		 * CustomeGestureDetector()).onTouchEvent(event); } });
		 */

		closeBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				closeBtn.setVisibility(View.GONE);
				hscroll.setVisibility(View.GONE);
				curlparams.weight = 6;
				mCurlView.setLayoutParams(curlparams);
			}
		});

	}

	/*
	 * private void loadWebViews(final int i) { WebView www = new WebView(this);
	 * 
	 * LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(100,
	 * 100);
	 * 
	 * 
	 * LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
	 * LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
	 * 
	 * 
	 * params2.setMargins(5, 5, 5, 5);
	 * 
	 * www.setLayoutParams(params2);
	 * 
	 * www.setInitialScale(1);
	 * 
	 * www.setId(i);
	 * 
	 * www.getSettings().setJavaScriptEnabled(true); // Set zoom
	 * enabled/disabled www.getSettings().setSupportZoom(true); // Support zoom
	 * like normal browsers www.getSettings().setUseWideViewPort(true); //
	 * Disable zoom buttons // www.getSettings().setDisplayZoomControls(false);
	 * // Add zoom controls www.getSettings().setBuiltInZoomControls(true); //
	 * Load the page on the maximum zoom out available.
	 * www.getSettings().setLoadWithOverviewMode(true);
	 * 
	 * www.setLongClickable(true); // www.setEnabled(false); //
	 * web.getSettings().setSupportMultipleWindows(true);
	 * 
	 * www.setWebViewClient(new WebViewClient() { public void
	 * onPageFinished(WebView view, String url) {
	 * 
	 * Picture picture = view.capturePicture();
	 * 
	 * Bitmap b = Bitmap.createBitmap(800, 1200, Bitmap.Config.ARGB_8888);
	 * 
	 * Canvas c = new Canvas(b);
	 * 
	 * picture.draw(c);
	 * 
	 * FileOutputStream fos = null;
	 * 
	 * try {
	 * 
	 * fos = new FileOutputStream("/sdcard/shambhi_images/" + i + ".jpg");
	 * 
	 * if (fos != null) { b.compress(Bitmap.CompressFormat.JPEG, 90, fos);
	 * 
	 * fos.close(); }
	 * 
	 * } catch (Exception e) { // ... }
	 * 
	 * } });
	 * 
	 * 
	 * int scale = 100 ; www.setInitialScale(scale);
	 * 
	 * www.getSettings().setSupportZoom(false);
	 * 
	 * 
	 * // www.loadUrl("file:///storage/emulated/0/MAGAZINE/" + urls[i]);
	 * 
	 * www.loadUrl(fiveURLS[i]);
	 * 
	 * favLinear.addView(www);
	 * 
	 * }
	 */

	@Override
	public void onPause() {
		super.onPause();
		mCurlView.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
		mCurlView.onResume();
	}

	@Override
	public Object onRetainNonConfigurationInstance() {
		return mCurlView.getCurrentIndex();
	}

	/**
	 * Bitmap provider.
	 */
	private class PageProvider implements CurlView.PageProvider {

		@Override
		public int getPageCount() {
			return fileList.size();
		}

		private Bitmap loadBitmap(int width, int height, int index) {
			Bitmap b = Bitmap.createBitmap(width, height,
					Bitmap.Config.ARGB_8888);
			/*
			 * b.eraseColor(0xFFFFFFFF); // Canvas c = new Canvas(b); Drawable d
			 * = getResources().getDrawable(mBitmapIds[index]);
			 * 
			 * // WebView wv=new WebView(CurlActivity.this);
			 * 
			 * int margin = 7; int border = 3; Rect r = new Rect(margin, margin,
			 * width - margin, height - margin);
			 * 
			 * int imageWidth = r.width() - (border * 2); int imageHeight =
			 * imageWidth * d.getIntrinsicHeight() / d.getIntrinsicWidth(); if
			 * (imageHeight > r.height() - (border * 2)) { imageHeight =
			 * r.height() - (border * 2); imageWidth = imageHeight *
			 * d.getIntrinsicWidth() / d.getIntrinsicHeight(); }
			 * 
			 * r.left += ((r.width() - imageWidth) / 2) - border; r.right =
			 * r.left + imageWidth + border + border; r.top += ((r.height() -
			 * imageHeight) / 2) - border; r.bottom = r.top + imageHeight +
			 * border + border;
			 * 
			 * Paint p = new Paint(); p.setColor(0xFFC0C0C0); c.drawRect(r, p);
			 * r.left += border; r.right -= border; r.top += border; r.bottom -=
			 * border;
			 * 
			 * d.setBounds(r); d.draw(c);
			 */

			return b;
		}

		private Bitmap loadBitmap(int width, int height, Bitmap bitmap) {
			Bitmap b = Bitmap.createBitmap(width, height,
					Bitmap.Config.ARGB_8888);
			b.eraseColor(0xFFFFFFFF);
			Canvas c = new Canvas(b);
			Drawable d = new BitmapDrawable(getResources(), bitmap);

			// WebView wv=new WebView(CurlActivity.this);

			int margin = 0;
			int border = 0;
			Rect r = new Rect(margin, margin, width - margin, height - margin);

			int imageWidth = r.width() - (border * 2);
			int imageHeight = imageWidth * d.getIntrinsicHeight()
					/ d.getIntrinsicWidth();
			if (imageHeight > r.height() - (border * 2)) {
				imageHeight = r.height() - (border * 2);
				imageWidth = imageHeight * d.getIntrinsicWidth()
						/ d.getIntrinsicHeight();
			}

			r.left += ((r.width() - imageWidth) / 2) - border;
			r.right = r.left + imageWidth + border + border;
			r.top += ((r.height() - imageHeight) / 2) - border;
			r.bottom = r.top + imageHeight + border + border;

			Paint p = new Paint();
			p.setColor(0xFFC0C0C0);
			c.drawRect(r, p);
			r.left += border;
			r.right -= border;
			r.top += border;
			r.bottom -= border;

			d.setBounds(r);
			d.draw(c);

			return b;
		}

		@Override
		public void updatePage(CurlPage page, final int width,
				final int height, int index) {

			pageIndex = index;

			/*
			 * String filepath = Environment.getExternalStorageDirectory()
			 * .getAbsolutePath() + "/MAGAZINE/MOVIE/" + urls[index]; File f =
			 * new File(filepath);
			 */
			try 
			{
				Bitmap front = BitmapFactory.decodeFile(fileList.get(index)
						.getAbsolutePath());
				if (Utility.magazineName.contains("Sai"))
					front = loadBitmap(width, height, front);

				/*
				 * Bitmap front =
				 * decodeScaledBitmapFromSdCard(fileList.get(index)
				 * .getAbsolutePath(), 500, 500);
				 */
				page.setTexture(front, CurlPage.SIDE_BOTH);
				page.setColor(Color.rgb(180, 180, 180), CurlPage.SIDE_BACK);
			}
			catch (OutOfMemoryError e) 
			{
				if (e != null)
					e.printStackTrace();
			}
			if ((mCurlView.getCurrentIndex()) % 5 == 0) {

				runOnUiThread(new Runnable() {
					public void run() {

						LayoutInflater inflater = getLayoutInflater();
						View layout = inflater
								.inflate(
										R.layout.toast_no_netowrk,
										(ViewGroup) findViewById(R.id.custom_toast_layout_id));

						Toast toast = new Toast(CurlActivity.this);
						toast.setDuration(Toast.LENGTH_LONG);
						toast.setView(layout);
						toast.show();

					}
				});

				// Toast.makeText(CurlActivity.this, "Double tap to zoom in !",
				// 2000).show();

				// showToast();

				// The Custom Toast Layout Imported here
				/*
				 * LayoutInflater inflater = getLayoutInflater(); View layout =
				 * inflater.inflate(R.layout.toast_no_netowrk, (ViewGroup)
				 * findViewById(R.id.custom_toast_layout_id));
				 * 
				 * 
				 * Toast toast = new Toast(CurlActivity.this);
				 * toast.setDuration(Toast.LENGTH_LONG); toast.setView(layout);
				 * toast.show();
				 */

			}

			// mCurlView.setFocusable(true);

			/*
			 * switch (index) { // First case is image on front side, solid
			 * colored back. case 0: {
			 * 
			 * 
			 * 
			 * 
			 * String filepath=Environment.getExternalStorageDirectory().
			 * getAbsolutePath()+"/MAGAZINE/MOVIE/"+urls[0]; File f=new
			 * File(filepath); Bitmap front
			 * =BitmapFactory.decodeFile(f.getAbsolutePath());
			 * 
			 * page.setTexture(front, CurlPage.SIDE_BOTH);
			 * page.setColor(Color.rgb(180, 180, 180), CurlPage.SIDE_BACK);
			 * 
			 * 
			 * 
			 * break; } // Second case is image on back side, solid colored
			 * front.
			 * 
			 * 
			 * String filepath=Environment.getExternalStorageDirectory().
			 * getAbsolutePath()+"/shambhi_images/image_1.png"; File f=new
			 * File(filepath); Bitmap front =
			 * BitmapFactory.decodeFile(f.getAbsolutePath());
			 * 
			 * shambhipage = page; Runnable runnable = new Runnable() {
			 * 
			 * @Override public void run() { handler.post(new Runnable() { //
			 * This thread runs in the // UI
			 * 
			 * @Override public void run() {
			 * 
			 * getWebViewScreen(width, height, 0);
			 * 
			 * File f = new File("/sdcard/yahoo_" + ".jpg"); Bitmap front =
			 * BitmapFactory.decodeFile(f .getAbsolutePath());
			 * 
			 * 
			 * page.setTexture(front, CurlPage.SIDE_FRONT);
			 * page.setColor(Color.rgb(180, 180, 180), CurlPage.SIDE_BACK);
			 * 
			 * 
			 * } }); } }; new Thread(runnable).start();
			 * 
			 * 
			 * 
			 * 
			 * 
			 * // Second case is image on back side, solid colored front. case
			 * 1: { // Bitmap back = loadBitmap(width, height, 2);
			 * 
			 * 
			 * 
			 * String filepath=Environment.getExternalStorageDirectory().
			 * getAbsolutePath()+"/MAGAZINE/MOVIE/"+urls[1]; File f=new
			 * File(filepath); Bitmap back
			 * =BitmapFactory.decodeFile(f.getAbsolutePath());
			 * 
			 * page.setTexture(back, CurlPage.SIDE_BOTH);
			 * page.setColor(Color.rgb(127, 140, 180), CurlPage.SIDE_BACK);
			 * break; } // Third case is images on both sides. case 2: { //
			 * Bitmap front = loadBitmap(width, height, 1);
			 * 
			 * 
			 * String filepath=Environment.getExternalStorageDirectory().
			 * getAbsolutePath()+"/MAGAZINE/MOVIE/"+urls[2]; File f=new
			 * File(filepath); Bitmap front
			 * =BitmapFactory.decodeFile(f.getAbsolutePath());
			 * 
			 * // Bitmap back = loadBitmap(width, height, 3);
			 * 
			 * 
			 * String filepath2=Environment.getExternalStorageDirectory().
			 * getAbsolutePath()+"/MAGAZINE/MOVIE/"+urls[3]; File f2=new
			 * File(filepath); Bitmap back
			 * =BitmapFactory.decodeFile(f.getAbsolutePath());
			 * 
			 * page.setTexture(front, CurlPage.SIDE_BOTH);
			 * page.setColor(Color.rgb(127, 140, 180), CurlPage.SIDE_BACK); //
			 * page.setTexture(back, CurlPage.SIDE_BACK); break; } // Fourth
			 * case is images on both sides - plus they are blend against //
			 * separate colors. case 3: { // Bitmap front = loadBitmap(width,
			 * height, 2);
			 * 
			 * String filepath=Environment.getExternalStorageDirectory().
			 * getAbsolutePath()+"/MAGAZINE/MOVIE/"+urls[3]; File f=new
			 * File(filepath); Bitmap front
			 * =BitmapFactory.decodeFile(f.getAbsolutePath());
			 * 
			 * // Bitmap back = loadBitmap(width, height, 1);
			 * 
			 * String filepath2=Environment.getExternalStorageDirectory().
			 * getAbsolutePath()+"/MAGAZINE/MOVIE/"+urls[1]; File f2=new
			 * File(filepath); Bitmap back
			 * =BitmapFactory.decodeFile(f.getAbsolutePath());
			 * 
			 * page.setTexture(front, CurlPage.SIDE_BOTH); //
			 * page.setTexture(back, CurlPage.SIDE_BACK); //
			 * page.setColor(Color.argb(127, 170, 130,
			 * 255),CurlPage.SIDE_FRONT); page.setColor(Color.rgb(255, 190,
			 * 150), CurlPage.SIDE_BACK); break; } // Fifth case is same image
			 * is assigned to front and back. In this // scenario only one
			 * texture is used and shared for both sides. case 4: // Bitmap
			 * front = loadBitmap(width, height, 0);
			 * 
			 * String filepath=Environment.getExternalStorageDirectory().
			 * getAbsolutePath()+"/MAGAZINE/MOVIE/"+urls[4]; File f=new
			 * File(filepath); Bitmap front
			 * =BitmapFactory.decodeFile(f.getAbsolutePath());
			 * 
			 * page.setTexture(front, CurlPage.SIDE_BOTH);
			 * page.setColor(Color.argb(127, 255, 255, 255),
			 * CurlPage.SIDE_BACK); break; }
			 */
		}

		private void showToast() {

			// The Custom Toast Layout Imported here
			LayoutInflater inflater = getLayoutInflater();
			View layout = inflater.inflate(R.layout.toast_no_netowrk,
					(ViewGroup) findViewById(R.id.custom_toast_layout_id));

			Toast toast = new Toast(CurlActivity.this);
			toast.setDuration(Toast.LENGTH_LONG);
			toast.setView(layout);
			toast.show();

		}

		private class CustomTask extends AsyncTask<Void, Void, Void> {

			protected void onPostExecute(Bitmap result) {

				shambhipage.setTexture(front, CurlPage.SIDE_FRONT);
				shambhipage.setColor(Color.rgb(180, 180, 180),
						CurlPage.SIDE_BACK);

			}

			@Override
			protected Void doInBackground(Void... params) {
				showToast();
				return null;

			}
		}

		private void getWebViewScreen(final int width, final int height, int i) {

			final WebView w = new WebView(CurlActivity.this);
			w.setInitialScale(1);

			w.getSettings().setJavaScriptEnabled(true);
			w.getSettings().setUseWideViewPort(true);
			w.getSettings().setLoadWithOverviewMode(true);
			w.getSettings().setSupportZoom(false);
			w.getSettings().setBuiltInZoomControls(false);
			w.setLongClickable(true);

			Bitmap b = null;
			w.setWebViewClient(new WebViewClient() {
				public void onPageFinished(WebView view, String url) {
					Picture picture = view.capturePicture();

					Bitmap b = Bitmap.createBitmap(800, 1200,
							Bitmap.Config.ARGB_8888);

					Canvas c = new Canvas(b);

					picture.draw(c);

					// b=loadBitmap(width, height, b);

					/*
					 * shambhipage.setTexture(b, CurlPage.SIDE_BACK);
					 * shambhipage.setColor(Color.rgb(127, 140, 180),
					 * CurlPage.SIDE_FRONT);
					 */

					FileOutputStream fos = null;

					try {

						fos = new FileOutputStream("/sdcard/111111" + ".jpg");

						if (fos != null) {
							b.compress(Bitmap.CompressFormat.JPEG, 90, fos);

							fos.close();
						}

					} catch (Exception e) {
						// ...
					}
					File f = new File("/sdcard/111111" + ".jpg");
					Bitmap front = BitmapFactory.decodeFile(f.getAbsolutePath());

					shambhipage.setTexture(front, CurlPage.SIDE_FRONT);
					shambhipage.setColor(Color.rgb(180, 180, 180),
							CurlPage.SIDE_BACK);

					/*
					 * FileOutputStream fos = null;
					 * 
					 * try {
					 * 
					 * fos = new FileOutputStream( "/sdcard/yahoo_" +
					 * System.currentTimeMillis() + ".jpg" );
					 * 
					 * if ( fos != null ) {
					 * b.compress(Bitmap.CompressFormat.JPEG, 90, fos );
					 * 
					 * fos.close(); }
					 * 
					 * } catch( Exception e ) { //... }
					 */
				}

			});

			// setContentView( w );

			// w.loadUrl( "file:///storage/emulated/0/MAGAZINE/"+urls[i]);

			// setContentView(w);

			// w.loadUrl( "file:///storage/emulated/0/MAGAZINE/"+urls[i]);

			w.loadUrl("http://www.yahoo.com");

			// return loadBitmap(width, height, b);
		}

	}

	public Bitmap getWebViewImage() {

		FileOutputStream fos = null;
		WebView w = new WebView(CurlActivity.this);
		w.loadUrl("http://www.yahoo.com");
		Bitmap mBitmap = null;
		ByteArrayOutputStream mByteArrayOpStream = null;

		// get the picture from the webview
		Picture picture = w.capturePicture();

		// Create the new Canvas
		Canvas mCanvas = new Canvas();

		// Copy the view canvas to a bitmap
		try {
			// w.draw(mCanvas);
			// mCanvas.save();
			// picture.draw(mCanvas);
			mCanvas.drawPicture(picture);
			// int restoreToCount =mCanvas.save();
			// mCanvas.drawPicture(picture);
			// mCanvas.restore();
		} catch (Exception e) {
			e.printStackTrace();
		}

		mBitmap = Bitmap.createBitmap(800, 1200, Config.ARGB_8888);
		mCanvas.drawBitmap(mBitmap, 0, 0, null);

		if (mBitmap != null) {
			mByteArrayOpStream = new ByteArrayOutputStream();
			mBitmap.compress(Bitmap.CompressFormat.JPEG, 90, mByteArrayOpStream);
			try {
				fos = openFileOutput("yahoo.jpg", MODE_WORLD_WRITEABLE);
				fos.write(mByteArrayOpStream.toByteArray());
				fos.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return mBitmap;

	}

	/**
	 * CurlView size changed observer.
	 */
	private class SizeChangedObserver implements CurlView.SizeChangedObserver {
		@Override
		public void onSizeChanged(int w, int h) {
			if (w > h) {
				mCurlView.setViewMode(CurlView.SHOW_TWO_PAGES);
				// mCurlView.setMargins(.1f, .05f, .1f, .05f);
			} else {
				mCurlView.setViewMode(CurlView.SHOW_ONE_PAGE);
				// mCurlView.setMargins(.1f, .1f, .1f, .1f);
			}
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(mCurlView.getCurrentIndex()<magazinesList.size())
		{
		gestureDetector.onTouchEvent(event);
		// scaleGestureDetector.onTouchEvent(event);
		
		}
		return false;
	}

	private class GestureListener extends
			GestureDetector.SimpleOnGestureListener {

		@Override
		public boolean onDown(MotionEvent e) {

			float ypos = ((screenHeight * 4) / 5);

			if (e.getY() > ypos) {
				if (hscroll.getVisibility() == View.VISIBLE) {
					hscroll.setVisibility(View.GONE);
					curlparams.weight = 6;
					mCurlView.setLayoutParams(curlparams);
					closeBtn.setVisibility(View.GONE);
				} else {
					curlparams.weight = 5;
					mCurlView.setLayoutParams(curlparams);
					hscroll.setVisibility(View.VISIBLE);
					closeBtn.setVisibility(View.VISIBLE);
				}
			} else {
				showWebview();
				mCurlView.setFocusable(true);
			}
			return true;
		}

		// event when double tap occurs
		@Override
		public boolean onDoubleTap(MotionEvent e) {
			float ypos = ((screenHeight * 4) / 5);

			if (e.getY() > ypos) {
				if (hscroll.getVisibility() == View.VISIBLE) {
					hscroll.setVisibility(View.GONE);
					curlparams.weight = 6;
					mCurlView.setLayoutParams(curlparams);
					closeBtn.setVisibility(View.GONE);
				} else {
					curlparams.weight = 5;
					mCurlView.setLayoutParams(curlparams);
					hscroll.setVisibility(View.VISIBLE);
					closeBtn.setVisibility(View.VISIBLE);
				}
			} else {
				showWebview();
				mCurlView.setFocusable(true);
			}

			return true;
		}
		/*
		 * @Override public boolean onFling(MotionEvent e1, MotionEvent e2,
		 * float velocityX, float velocityY) { if (e1 == null || e2 == null)
		 * return false; if (e1.getPointerCount() > 1 || e2.getPointerCount() >
		 * 1) return false; else { try { // right to left swipe .. go to next
		 * page if (e1.getX() - e2.getX() > 100 && Math.abs(velocityX) > 800) {
		 * messageDialog.cancel(); return true; } // left to right swipe .. go
		 * to prev page else if (e2.getX() - e1.getX() > 100 &&
		 * Math.abs(velocityX) > 800) { messageDialog.cancel(); return true; }
		 * // bottom to top, go to next document else if (e1.getY() - e2.getY()
		 * > 100 && Math.abs(velocityY) > 800 && web3.getScrollY() >=
		 * web3.getScale() (web3.getContentHeight() - web3 .getHeight())) { if
		 * (hscroll.getVisibility() == View.VISIBLE) {
		 * hscroll.setVisibility(View.GONE); curlparams.weight=6;
		 * mCurlView.setLayoutParams(curlparams); } else { curlparams.weight=5;
		 * mCurlView.setLayoutParams(curlparams);
		 * hscroll.setVisibility(View.VISIBLE); } // do your stuff return true;
		 * } // top to bottom, go to prev document else if (e2.getY() -
		 * e1.getY() > 100 && Math.abs(velocityY) > 800) { if
		 * (hscroll.getVisibility() == View.VISIBLE) {
		 * hscroll.setVisibility(View.GONE); curlparams.weight=6;
		 * mCurlView.setLayoutParams(curlparams); } else { curlparams.weight=5;
		 * mCurlView.setLayoutParams(curlparams);
		 * hscroll.setVisibility(View.VISIBLE); } // do your stuff return true;
		 * } } catch (Exception e) { // nothing } return false; } }
		 */
	}

	private void showWebview() {

		simpleCount = 1;

		LayoutInflater inflater = LayoutInflater.from(CurlActivity.this);
		View helpDialogView = (View) inflater.inflate(R.layout.page1, null,
				false);
		messageDialog = new Dialog(CurlActivity.this,
				android.R.style.Theme_Translucent_NoTitleBar); // old
																// Theme_Translucent_NoTitleBar
																// //
		Window window = messageDialog.getWindow();
		window.setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND,
				WindowManager.LayoutParams.FLAG_DIM_BEHIND);

		// window.setLayout(screenWidth, screenHeight);
		messageDialog.setCancelable(true);
		messageDialog.setContentView(helpDialogView);
		messageDialog.show();
		detector = new GestureDetector(new MyGestureDetector());
		web3 = (CustomWebView) helpDialogView.findViewById(R.id.webView1);

		web3.setFocusable(true);
		web3.setInitialScale(1);
		web3.getSettings().setJavaScriptEnabled(true);
		web3.getSettings().setUseWideViewPort(true);
		web3.getSettings().setBuiltInZoomControls(true);
		web3.getSettings().setLoadWithOverviewMode(true);
		web3.getSettings().setSupportMultipleWindows(true);

		web3.setWebViewClient(new WebViewClient() {

			@Override
			public boolean shouldOverrideUrlLoading(WebView view,
					String stringUrl) {
				// web3.setFocusable(true);
				return true;

			}
		});
		/*
		 * final GestureDetector gd = new GestureDetector(new
		 * MyGestureDetector()); View.OnTouchListener gl = new
		 * View.OnTouchListener() { public boolean onTouch(View v, MotionEvent
		 * event) {
		 * 
		 * // pageTurmHere(event);
		 * 
		 * 
		 * gd.onTouchEvent(event); // web3.setFocusable(true); //
		 * web3.setFocusableInTouchMode(true); return false; } };
		 */
		/*
		 * web3.setOnTouchListener(new View.OnTouchListener() {
		 * 
		 * @Override public boolean onTouch(View v, MotionEvent event) {
		 * mCurlView.setFocusable(false); web3.setFocusable(true);
		 * web3.setFocusableInTouchMode(true); v.requestFocus(); gd = new
		 * GestureDetector(new MyGestureDetector());
		 * 
		 * return gd.onTouchEvent(event); } });
		 */
		web3.setGestureDetector(new GestureDetector(
				new CustomeGestureDetector()));
		if(mCurlView.getCurrentIndex()<magazinesList.size())
		{
		String webfilePath = "file://"
				+ Configuration.getMagazinesDirectory(this) + "/"
				+ jsonBook.getMagazineName() + File.separator
				+ magazinesList.get(mCurlView.getCurrentIndex());
		web3.loadUrl(webfilePath);
		}

	}

	private class CustomeGestureDetector extends SimpleOnGestureListener {
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			if (e1 == null || e2 == null)
				return false;
			if (e1.getPointerCount() > 1 || e2.getPointerCount() > 1)
				return false;
			else {
				try { // right to left swipe .. go to next page
					if (e1.getX() - e2.getX() > 100
							&& Math.abs(velocityX) > 500) {
						simpleCount = 0;
						messageDialog.cancel();
						return true;
					} // left to right swipe .. go to prev page
					else if (e2.getX() - e1.getX() > 100
							&& Math.abs(velocityX) > 500) {
						simpleCount = 0;
						messageDialog.cancel();
						return true;
					} // bottom to top, go to next document
					else if (e1.getY() - e2.getY() > 100
							&& Math.abs(velocityY) > 800
							&& web3.getScrollY() >= web3.getScale()
									* (web3.getContentHeight() - web3
											.getHeight())) {
						if (hscroll.getVisibility() == View.VISIBLE) {
							hscroll.setVisibility(View.GONE);
							curlparams.weight = 6;
							mCurlView.setLayoutParams(curlparams);
							closeBtn.setVisibility(View.GONE);
						} else {
							curlparams.weight = 5;
							mCurlView.setLayoutParams(curlparams);
							hscroll.setVisibility(View.VISIBLE);
							closeBtn.setVisibility(View.VISIBLE);
						}
						// do your stuff
						return true;
					} // top to bottom, go to prev document
					else if (e2.getY() - e1.getY() > 100
							&& Math.abs(velocityY) > 800) {

						if (hscroll.getVisibility() == View.VISIBLE) {
							hscroll.setVisibility(View.GONE);
							curlparams.weight = 6;
							mCurlView.setLayoutParams(curlparams);
							closeBtn.setVisibility(View.GONE);
						} else {
							curlparams.weight = 5;
							mCurlView.setLayoutParams(curlparams);
							hscroll.setVisibility(View.VISIBLE);
							closeBtn.setVisibility(View.VISIBLE);
						}
						// do your stuff

						return true;
					}
				} catch (Exception e) { // nothing
				}
				return false;
			}
		}
	}

	class MyGestureDetector extends SimpleOnGestureListener {
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {

			try {

				// right to left swipe
				if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
						&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {

					// page.next();

					float swipeValue = (screenWidth * (4 / 5));

					if (e1.getX() > swipeValue) {
						messageDialog.cancel();
					}

					return true;
				} else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
						&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {

					float swipeValue = (screenWidth * (1 / 5));

					if (e1.getX() < swipeValue) {
						messageDialog.cancel();
					}

					messageDialog.cancel();

					// page.pre();
					return true;
				}
			}

			catch (Exception e) {
				e.printStackTrace();
			}

			return true;
		}
	}

	/*
	 * @Override public boolean dispatchTouchEvent(MotionEvent ev) {
	 * gestureDetector.onTouchEvent(ev); return false; }
	 */

	private void setDimensions() {
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		screenHeight = displaymetrics.heightPixels;
		screenWidth = displaymetrics.widthPixels;
	}

	@Override
	public void onClick(View v) {
		mCurlView.setCurrentIndex(v.getId());

	}

	public class simpleOnScaleGestureListener extends
			SimpleOnScaleGestureListener {

		@Override
		public boolean onScale(ScaleGestureDetector detector) {
			if (simpleCount == 0) {
				showWebview();
				mCurlView.setFocusable(true);
			}
			return true;
		}

		@Override
		public boolean onScaleBegin(ScaleGestureDetector detector) {
			// TODO Auto-generated method stub

			return true;
		}

		@Override
		public void onScaleEnd(ScaleGestureDetector detector) {
			// TODO Auto-generated method stub

		}

	}

	public Bitmap decodeScaledBitmapFromSdCard(String filePath, int reqWidth,
			int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(filePath, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeFile(filePath, options);
	}

	public int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and
			// width
			final int heightRatio = Math.round((float) height
					/ (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will
			// guarantee
			// a final image with both dimensions larger than or equal to
			// the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}
}